﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhorcadosAscii
{
    class Score
    {
        public List<User> userScores;

        public Score()
        {
            this.userScores = new List<User>();
        }

        public void SaveScore(User user)
        {
            userScores.Add(user);
        }

        public void ShowScores()
        {
            Console.WriteLine("User\t\t\tNumero intentos");
            if (userScores.Count > 0)
            {
                foreach (User user in userScores)
                {
                    Console.WriteLine("{0}\t\t\t{1}", user.name, user.nroMovimientos);
                }
            }
        }
    }
}
