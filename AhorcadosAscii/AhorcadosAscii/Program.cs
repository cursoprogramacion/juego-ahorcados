﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhorcadosAscii
{
    public class Program
    {
        public static void Main()
        {
            string tempStr;
            byte opc;
            AsciiMenus asciiMenu = new AsciiMenus();            
            List<User> users = new List<User>();

            GameLogic game = new GameLogic();

            // Preguntas y respuesta sistema solar
            string[,] sistemaSolar = new string[9, 2] {
                {"Un planeta del sistema solar distitno de saturno que tambien tiene aniños", "urano"},
                {"Es el primer planeta del sistema solar", "mercurio"},
                {"Esta luna tiene el nombre de un continente de la tierra", "europa"},
                {"¿Como se llama el satelite natural de la tierra?", "luna"},
                {"Es el planeta inmediatamente siguiente a la tierra si viajaramos al sol", "venus"},
                {"En palabras de Carl Sagan, este planeta es un sol fracasado", "jupiter"},
                {"Es la unica luna conocida del sistema solar con atmosfera densa, y estudios recientes indican que la vida alli en forma de moleculas organicas es posible", "titan"},
                {"Es la luna mas grande del sistema solar", "gaminedes"},
                {"Este planeta se observa de un colo azul oscuro desde el espacio", "neptuno"}
            };

            // Preguntas y respuesta sistema solar
            string[,] grandesAstronomos = new string[9, 2] {
                {"Este astronomo concibio la redondez de la tierra", "mileto"},
                {"Fue el primer astrónomo de quien se tiene noticia", "anaximandro"},
                {"Calculó la circunferencia terrestre considerando que la Tierra es esférica y los rayos del Sol son paralelos cuando llegan a la Tierra.", "erastotenes"},
                {"Consideró al sol en el centro de todas las órbitas planetarias", "copernico"},
                {"Demostró que los planetas no siguen una órbita circular sino elíptica respecto del Sol en un foco del elipse", "kepler"},
                {"Estableció la ley de la Gravitación Universal", "newton"},
                {"Desarrolló la teoría de la Relatividad General y Especial.", "einstein"},
                {"Demostro la expansión del universo midiendo", "hubble"},
                {"Es el escritor de Cosmos, un famoso libro de divulgacion cientifica", "sagan"}
            };

            do
            {
                User user = new User();                              

                // MAIN MENU ################################
                asciiMenu.MainMenu();
                tempStr = Console.ReadLine();

                if (byte.TryParse(tempStr, out opc))
                {
                    switch (opc)
                    {
                        // Partida nueva
                        case 1:
                            do
                            {
                                asciiMenu.SelectCategory();
                                tempStr = Console.ReadLine();

                                if (byte.TryParse(tempStr, out opc))
                                {
                                    switch (opc)
                                    {
                                        // Select topic
                                        case 1:
                                            asciiMenu.SelectTopic();
                                            do
                                            {
                                                tempStr = Console.ReadLine();
                                                if (byte.TryParse(tempStr, out opc))
                                                {
                                                    Random rnd = new Random();
                                                    switch (opc)
                                                    { 
                                                        // Sistema solar.
                                                        case 1:
                                                            Console.WriteLine("Se escojio categoria: Sistema solar");
                                                            game.SetPregunta(sistemaSolar, rnd.Next( sistemaSolar.GetLength(0) ));
                                                            game.RunLogic("cuerpo celeste", ref user);

                                                            asciiMenu.InsertName();
                                                            user.InputName();
                                                            users.Add(user);

                                                            opc = 3;
                                                            break;
                                                        // Telescopios espaciales.
                                                        case 2:
                                                            Console.WriteLine("Se escojio categoria: Grandes astronomos");
                                                            game.SetPregunta(grandesAstronomos, rnd.Next(grandesAstronomos.GetLength(0)));
                                                            game.RunLogic("astronomo", ref user);

                                                            asciiMenu.InsertName();
                                                            user.InputName();
                                                            users.Add(user);

                                                            opc = 3;
                                                            break;
                                                        // Volver al menu anterior
                                                        case 3:
                                                            break;
                                                    }
                                                }
                                            } while (opc !=3);
                                            break;

                                        // Categoria aleatoria
                                        case 2:
                                            Random category;                                           
                                            category = new Random();

                                            opc = (byte) category.Next(1, 3); // Numero entero aleatorio entre 1 y 2
                                            do
                                            {
                                                Random rnd = new Random();
                                                switch (opc)
                                                {
                                                    // Sistema solar.
                                                    case 1:
                                                        Console.WriteLine("Se escojio categoria: Sistema solar");
                                                        game.SetPregunta(sistemaSolar, rnd.Next(sistemaSolar.GetLength(0)));
                                                        game.RunLogic("cuerpo celeste", ref user);

                                                        asciiMenu.InsertName();
                                                        user.InputName();
                                                        users.Add(user);

                                                        opc = 3;
                                                        break;
                                                    // Telescopios espaciales.
                                                    case 2:
                                                        Console.WriteLine("Se escojio categoria: Grandes astronomos");
                                                        game.SetPregunta(grandesAstronomos, rnd.Next(grandesAstronomos.GetLength(0)));
                                                        game.RunLogic("astronomo", ref user);

                                                        asciiMenu.InsertName();
                                                        user.InputName();
                                                        users.Add(user);

                                                        opc = 3;
                                                        break;
                                                    // Volver al menu anterior
                                                    case 3:
                                                        break;
                                                }
                                            } while (opc != 3);
                                            break;


                                        case 3:
                                            Console.WriteLine("Salgó al menú principal");
                                            break;
                                        // Empieza el juego
                                        case 37:
                                            break;
                                        default:
                                            asciiMenu.Error();
                                            break;
                                    }
                                }
                                else
                                {

                                }

                            } while (opc != 3);
                            opc = 6; // Se cambia el valor antes de salir para garantizar que si se devuelve no se salga del juego
                            break;

                        // Tabla de posiciones
                        case 2:
                            Console.WriteLine("User\t\t\tIntentos Restantes");
                            if (users.Count > 0)
                            {
                                List<int> intentosRestantesList = new List<int>();
                                foreach (User userIntentosRestantes in users)
                                {
                                    intentosRestantesList.Add(userIntentosRestantes.intentosRestantes);
                                }
                                intentosRestantesList.Sort();
                                // Los usuarios no se muestran en orden
                                foreach (User usr in users)
                                {
                                    Console.WriteLine("{0}\t\t\t{1}", usr.name, usr.intentosRestantes);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Aun nadie ha jugado");
                            }
                            Console.WriteLine("Presione alguna tecla para volver");
                            Console.ReadKey();
                            opc = 6;
                            break;

                        // Salir
                        case 3:
                            break;

                        // Error
                        default:
                            asciiMenu.Error();
                            break;

                    }
                }
                else
                {
                    Console.WriteLine("No ingresaste un número correcto");
                }

            } while (opc != 3);
        }
    }    
}
