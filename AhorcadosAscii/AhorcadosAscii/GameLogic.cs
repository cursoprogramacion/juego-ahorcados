﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhorcadosAscii
{
    class GameLogic
    {
        private string pregunta;
        private string respuesta;
        private string respuestaAdaptada;


        private byte maxWordLength = 1;
        private string intento;
        private string progreso;

        private char[] serpiente;

        public GameLogic()
        {
            serpiente = new char[10];
            for (int i = 0; i < serpiente.Length; i++)
            {
                serpiente[i] = '=';
            }
        }

        public void RunLogic(string planetaOAstronomo, ref User usr)
        {
            this.progreso = "";
            for (byte i = 0; i < this.respuesta.Length; i++)
            {
                this.progreso += "_";
            }
            this.progreso = string.Join(" ", progreso.ToCharArray());
            Console.WriteLine(this.pregunta);
            Console.Write("R/ Este {0} es: {1}", planetaOAstronomo, progreso);
            Console.WriteLine("");


            this.NormalizarRespuesta();
            this.respuestaAdaptada.Trim(); // Quita espacios del final y del principio
            this.progreso.Trim(); // Quita espacios del final y del principio

            char[] charsRespuestaAdptada = this.respuestaAdaptada.ToCharArray();
            char[] chars = this.progreso.ToCharArray();

            while (!this.respuestaAdaptada.Equals(this.progreso, StringComparison.OrdinalIgnoreCase) && (usr.intentosRestantes > 0))
            {
                // Console.WriteLine(user.nroMovimientos);
                // ACA SE PUEDE USAR EL METODO Array.IndexOf<char>(charRespuestaAdaptada, intento[0])
                // Console.WriteLine(charsRespuestaAdptada.Length);
                this.InsertarIntento();
                usr.nroMovimientos++;
                usr.nroAciertos = 0;
                for (int i = 0; i < charsRespuestaAdptada.Length; i++)
                {
                    if (this.intento[0] == charsRespuestaAdptada[i])
                    {
                        chars[i] = this.intento[0];
                        this.progreso = new string(chars);
                        usr.nroAciertos++;
                    }
                }
                Console.WriteLine(this.progreso + "\n");

                if (usr.nroAciertos == 0)
                {
                    this.serpiente[ usr.intentosRestantes - 1 ] = 'x';
                    Console.WriteLine(string.Join(" ", serpiente) + "\n");
                    usr.intentosRestantes--;
                }
            }
            if (usr.intentosRestantes > 0)
            {
                Console.WriteLine("Que bien, lo has conseguido!!!");
            }
            else
            {
                Console.WriteLine("Bueno, no lo has logrado, pero dame tu nombre igualmente");
            }
        }

        public void SetPregunta(string[,] preg, int index)
        {
            // TO-DO: Estudiar como capturar los posibles errores q se puedan producir en este metodo.
            if (index >= 0 && index <= 9)
            {
                this.pregunta = preg[index, 0];
                this.respuesta = preg[index, 1];
            }
            else
            {
                Console.WriteLine("Asegurese q halla suficientes preguntas");
            }
        }

        // Agrega un espacio entre cada una de las letras de la respuesta
        private void NormalizarRespuesta()
        {
            this.respuestaAdaptada = String.Join(" ", this.respuesta.ToCharArray());
        }

        // Input word
        private void InsertarIntento()
        {
            string TempStr;
            bool TempBin;

            do
            {

                TempStr = Console.ReadLine();

                if (TempStr.Length <= this.maxWordLength)
                {
                    this.intento = TempStr;
                    TempBin = true;
                }
                else
                {
                    Console.WriteLine("(Solo puedes introducir de a una sola letra a la vez)\n");
                    TempBin = false;
                }

            } while (!TempBin);
        }
    }
}
