﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhorcadosAscii
{
    class AsciiMenus
    {
        public void MainMenu()
        {
            Console.WriteLine("\n\n                                                    ___ ");
            Console.WriteLine("          Ahorcados Planetario                    ,o88888 ");
            Console.WriteLine("                                               ,o8888888\' ");
            Console.WriteLine("                         ,:o:o:oooo.        ,8O88Pd8888\" ");
            Console.WriteLine("                     ,.::.::o:ooooOoOoO. ,oO8O8Pd888\'\" ");
            Console.WriteLine("                   ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O\" ");
            Console.WriteLine("                  , ..:.::o:ooOoOOOO8OOOOo.FdO8O8\" ");
            Console.WriteLine("                 , ..:.::o:ooOoOO8O888O8O,COCOO\" ");
            Console.WriteLine("                , . ..:.::o:ooOoOOOO8OOOOCOCO\" ");
            Console.WriteLine("                 . ..:.::o:ooOoOoOO8O8OCCCC\"o ");
            Console.WriteLine("                    . ..:.::o:ooooOoCoCCC\"o:o ");
            Console.WriteLine("                    . ..:.::o:o:,cooooCo\"oo:o: ");
            Console.WriteLine("                 `   . . ..:.:cocoooo\"\'o:o:::\' ");
            Console.WriteLine("                 .`   . ..::ccccoc\"\'o:o:o:::\' ");
            Console.WriteLine("                :.:.    ,c:cccc\"\':.:.:.:.:.\' ");
            Console.WriteLine("              ..:.:\"\'`::::c:\"\'..:.:.:.:.:.\' ");
            Console.WriteLine("            ...:.\'.:.::::\"\'    . . . . .\' ");
            Console.WriteLine("           .. . ....:.\"\' `   .  . . \'\' ");
            Console.WriteLine("         . . . ....\"\' ");
            Console.WriteLine("         .. . .\"\'     -Game by: @gamebite10- ");
            Console.WriteLine("        . \n\n");

            Console.WriteLine("           1. Partida nueva.");
            Console.WriteLine("           2. Tabla de posiciones.");
            Console.WriteLine("           3. Salir.");
        }

        public void Error()
        {
            Console.WriteLine("\n\n                    #####");
            Console.WriteLine("                   #### _\\_  ________");
            Console.WriteLine("                   ##=-[.].]| \\      \\");
            Console.WriteLine("                   #(    _\\ |  |------|");
            Console.WriteLine("                    #   __| |  ||||||||");
            Console.WriteLine("                     \\  _/  |  ||||||||");
            Console.WriteLine("                  .--\'--\'-. |  | ____ |");
            Console.WriteLine("                 / __      `|__|[o__o]|");
            Console.WriteLine("               _(____nm_______ /____\\____ \\n\\n");

            Console.WriteLine("             No, no, no, Opcion Incorrecta  ");
            Console.WriteLine("             ¿Estas bromeando conmigo?  ");
        }

        public void SelectCategory()
        {
            Console.WriteLine("\n\no               .        ___---___                    .");
            Console.WriteLine("       .              .--\\        --.     .     .         .");
            Console.WriteLine("                    ./.;_.\\     __/~ \\.");
            Console.WriteLine("                   /;  / `-\'  __\\    . \\");
            Console.WriteLine(" .        .       / ,--\'     / .   .;   \\        |");
            Console.WriteLine("                 | .|       /       __   |      -O-       .");
            Console.WriteLine("                |__/    __ |  . ;   \\ | . |      |");
            Console.WriteLine("                |      /  \\\\_    . ;| \\___|");
            Console.WriteLine("   .    o       |      \\  .~\\\\___,--\'     |           .");
            Console.WriteLine("                 |     | . ; ~~~~\\_    __|");
            Console.WriteLine("    |             \\    \\   .  .  ; \\  /_/   .");
            Console.WriteLine("   -O-        .    \\   /         . |  ~/                  .");
            Console.WriteLine("    |    .          ~\\ \\   .      /  /~          o");
            Console.WriteLine("  .                   ~--___ ; ___--~");
            Console.WriteLine("                 .          ---         .              -JT\n\n");
            Console.WriteLine("                     -Selecciona una categoria-");
            Console.WriteLine("        1. Seleccionar categoria (2 categorias disponibles).");
            Console.WriteLine("        2. Categoria aleatoria.");
            Console.WriteLine("        3. Salir.");
        }

        public void SelectTopic()
        {
            Console.WriteLine("                ____");
            Console.WriteLine("                /___.`--.____ .--. ____.--(");
            Console.WriteLine("                       .\'_.- (    ) -._\'.");
            Console.WriteLine("                     .\'.\'    |\'..\'|    \'.\'.");
            Console.WriteLine("              .-.  .\' /\'--.__|____|__.--\'\\ \'.  .-.");
            Console.WriteLine("             (O).)-| |  \\    |    |    /  | |-(.(O)");
            Console.WriteLine("              `-\'  \'-\'-._\'-./      \\.-\'_.-\'-\'  `-\'");
            Console.WriteLine("                 _ | |   \'-.________.-\'   | | _");
            Console.WriteLine("              .\' _ | |     |   __   |     | | _ \'.");
            Console.WriteLine("             / .\' \'\'.|     | /    \\ |     |.\'\' \'. \\");
            Console.WriteLine("             | |( )| \'.    ||      ||    .\' |( )| |");
            Console.WriteLine("             \\ \'._.\'   \'.  | \\    / |  .\'   \'._.\' /");
            Console.WriteLine("              \'.__ ______\'.|__\'--\'__|.\'______ __.\'");
            Console.WriteLine("             .\'_.-|         |------|         |-._\'.");
            Console.WriteLine("            //\\\\  |         |--::--|         |  //\\\\");
            Console.WriteLine("           //  \\\\ |         |--::--|         | //  \\\\");
            Console.WriteLine("          //    \\\\|        /|--::--|\\        |//    \\\\");
            Console.WriteLine("         / \'._.-\'/|_______/ |--::--| \\_______|\\`-._.\' \\");
            Console.WriteLine("        / __..--\'        /__|--::--|__\\        `--..__ \\");
            Console.WriteLine("       / /               \'-.|--::--|.-\'               \\ \\");
            Console.WriteLine("      / /                   |--::--|                   \\ \\");
            Console.WriteLine("     / /                    |--::--|                    \\ \\");
            Console.WriteLine(" _.-\'  `-._                 _..||.._                  _.-` \'-._");
            Console.WriteLine("\'--..__..--\' LGB           \'-.____.-\'                \'--..__..-\'");

            Console.WriteLine("\n\n          1. Sistema solar");
            Console.WriteLine("          2. Grandes astronomos");
            Console.WriteLine("          3. Volver");

        }

        public void InsertName()
        {
            Console.WriteLine("          \\\\\\|||///               \\\\\\|||///                \\\\\\|||///");
            Console.WriteLine("        .  =======              .  =======               .  =======");
            Console.WriteLine("       / \\| O   O |            / \\| O   O |             / \\| O   O |");
            Console.WriteLine("       \\ /  \\v_\'/              \\ /  \\v_\'/               \\ /  \\v_\'/");
            Console.WriteLine("        #   _| |_               #   _| |_                #   _| |_");
            Console.WriteLine("       (#) (     )             (#) (     )              (#) (     )");
            Console.WriteLine("        #\\//|* *|\\\\             #\\//|* *|\\\\              #\\//|* *|\\\\");
            Console.WriteLine("        #\\/(  *  )/             #\\/(  *  )/              #\\/(  *  )/");
            Console.WriteLine("        #   =====               #   =====                #   =====");
            Console.WriteLine("        #   (\\ /)               #   (\\ /)                #   (\\ /)");
            Console.WriteLine("        #   || ||               #   || ||                #   || ||");
            Console.WriteLine("       .#---\'| |----.          .#---\'| |----.           .#---\'| |----.");
            Console.WriteLine("        #----\' -----\'           #----\' -----\'            #----\' -----\'\n\n");
            Console.WriteLine("                               Dime tu nombre (Maximo 10 caracteres)");
        }
    }
}
