﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhorcadosAscii
{
    class User
    {
        public string name;
        public int nroMovimientos = 0; // Cuantas veces tuvo q presionar una tecla para adivinar la palabra
        public int nroAciertos;
        public int intentosRestantes = 10;
        private byte maxLengthName = 10;

        // Input
        public void InputName()
        {
            string TempStr;
            bool TempBin;

            do
            {

                TempStr = Console.ReadLine();

                if (TempStr.Length <= this.maxLengthName)
                {
                    this.name = TempStr;
                    TempBin = true;
                }
                else
                {
                    Console.WriteLine("Tu nombre es muy largo, no lo podre recordar, intenta de nuevo:");
                    TempBin = false;
                }

            } while (!TempBin);
        }
    }
}
